//
//  SettingsStorage.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-04-13.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

class SettingsStorage {
    
    private static let URLS_DEFAULT_KEY = "toboggan.urls"
    static var urls: [String] {
        get {
            return (UserDefaults.standard.array(forKey: URLS_DEFAULT_KEY) as? [String]) ?? []
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: URLS_DEFAULT_KEY)
        }
    }
    
    private static let DEFAULT_URL_DEFAULT_KEY = "toboggan.default-url"
    static var defaultUrl: String? {
        get {
            return UserDefaults.standard.string(forKey: DEFAULT_URL_DEFAULT_KEY)
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: DEFAULT_URL_DEFAULT_KEY)
        }
    }
}
