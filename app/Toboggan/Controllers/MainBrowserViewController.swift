//
//  ViewController.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-03-22.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit
import WebKit

class MainBrowserViewController: UIViewController, WKNavigationDelegate, UrlsViewControllerDelegate {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var homeBarButton: UIBarButtonItem!
    @IBOutlet weak var previousBarButton: UIBarButtonItem!
    @IBOutlet weak var nextBarButton: UIBarButtonItem!
    @IBOutlet weak var listBarButton: UIBarButtonItem!
    
    private var contentLoaded: Bool = false
    private var shouldReloadForAuth = false
    private var cancelledAuth = false
    
    private var refreshControl: UIRefreshControl {
        get {
            return webView.scrollView.refreshControl!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        
        webView.scrollView.refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onRefresh(_:)), for: .valueChanged)
        
        previousBarButton.isEnabled = false
        nextBarButton.isEnabled = false
        
        loadUrl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (SettingsStorage.defaultUrl == nil) {
            showOkAlert(title: "Hello :)", message: "First, you must configure the URLs to use within the browser.") {
                self.performSegue(withIdentifier: "segue.popover.urlsviewcontroller", sender: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "segue.pdfviewer") {
            let pdfViewer = segue.destination as! PdfViewerViewController
            pdfViewer.pdfUrl = sender as! URL
        } else if (segue.identifier == "segue.popover.urlsviewcontroller") {
            let urlsController = (segue.destination as! UINavigationController).childViewControllers.first! as! UrlsViewController
            urlsController.delegate = self
        }
    }
    
    @objc private func onRefresh(_ sender: UIRefreshControl) {
        webView.reload()
    }
    
    //
    // Methods
    //
    
    private func loadUrl() {
        webView.stopLoading()
        
        if  let defaultUrl = SettingsStorage.defaultUrl,
            let url = URL(string: defaultUrl) {
                webView.load(URLRequest(url: url))
            
                startRefreshing()
        }
    }
    
    private func startRefreshing() {
        DispatchQueue.main.asyncAfter(deadline: (.now() + .milliseconds(300)), execute: {
            // Display refeshControl if loading takes more than 300ms
            if (self.contentLoaded) {
                self.endRefreshing()
            } else {
                self.refreshControl.beginRefreshing()
            }
        })
    }
    
    private func endRefreshing() {
        DispatchQueue.main.asyncAfter(deadline: (.now() + .milliseconds(300)), execute: {
            self.refreshControl.endRefreshing()
        })
    }
    
    private func updateNavigationButtons() {
        previousBarButton.isEnabled = webView.canGoBack
        nextBarButton.isEnabled = webView.canGoForward
    }
    
    private func showNavigationError(error: String, url: String?) {
        let urlMessage = (url == nil ? "" : "\nfor URL \(url!)")
        let genericMessage = "(Make sure the selected URL in the list is valid.)"
        showOkAlert(title: "Error", message: "Navigation failed:\n\(error)\(urlMessage)\n\(genericMessage)")
    }
    
    //
    // IBActions
    //
    
    @IBAction func onHomeTapped(_ sender: Any) {
        loadUrl()
    }
    
    @IBAction func onPreviousTapped(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func onNextTapped(_ sender: Any) {
        webView.goForward()
    }
    
    //
    // UrlsViewControllerDelegate
    //
    
    func selectedUrlChanged() {
        loadUrl()
    }
    
    //
    // WKNavigationDelegate
    //
    
    // Should we navigate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("decidePolicyFor navigationAction")
        
        if let url = navigationAction.request.url {
            if (navigationAction.targetFrame == nil) {
                //new window, so force loading the url in the current webview
                decisionHandler(.cancel)
                
                webView.load(URLRequest(url: url))
            } else if (url.pathExtension.lowercased() == "pdf") {
                decisionHandler(.cancel)
                
                performSegue(withIdentifier: "segue.pdfviewer", sender: url)
            } else if (url.scheme?.lowercased().starts(with: "http") == false
                && url.scheme?.lowercased().starts(with: "ftp") == false) {
                if (UIApplication.shared.canOpenURL(url)) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    showNavigationError(error: "Unrecognized protocol.", url: "\(url)")
                }
                
                decisionHandler(.cancel)
            } else {
                // standard web navigation
                decisionHandler(.allow)
            }
        } else {
            showNavigationError(error: "Invalid URL request.", url: nil)
            
            decisionHandler(.cancel)
        }
    }
    
    // Navigation started
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        contentLoaded = false
        startRefreshing()
    }
    
    // Navigation was interrupted
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        endRefreshing()
        
        if (cancelledAuth) {
            cancelledAuth = false
        } else {
            showNavigationError(error: error.localizedDescription, url: nil)
        }
        
        print("didFailProvisionalNavigation navigation: \n\(error.localizedDescription)")
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        endRefreshing()
        
        showNavigationError(error: error.localizedDescription, url: nil)
    }
    
    // A response was received from navigation, contente not loaded in webview yet
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
        
        if let httpResponse = navigationResponse.response as? HTTPURLResponse {
            if (httpResponse.statusCode == 401) {
                shouldReloadForAuth = true
            }
        }
        
        print("decidePolicyFor navigationResponse \(navigationResponse.response.url!)")
    }

    // Navigation completed, content loaded in webview
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        contentLoaded = true
        self.navigationItem.title = webView.title ?? webView.url!.host!
        
        endRefreshing()
        
        if (shouldReloadForAuth) {
            shouldReloadForAuth = false
            
            webView.reload()
        }
        
        updateNavigationButtons()
    }
    
    // A authentication challenge was received
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        print("URLAuthenticationChallenge \(challenge.protectionSpace.authenticationMethod)")
        
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic) {
            cancelledAuth = false
            
            let loginAlert = UIAlertController(title: "Authorization required",
                                               message: "You must authenticate yourself to access this URL.",
                                               preferredStyle: .alert)
            loginAlert.addTextField { textfield in
                textfield.placeholder = "Username"
            }
            loginAlert.addTextField { textfield in
                textfield.placeholder = "Password"
                textfield.isSecureTextEntry = true
            }
            loginAlert.addAction(UIAlertAction(title: "Login", style: UIAlertActionStyle.default, handler: {_ in
                let username = loginAlert.textFields![0].text ?? ""
                let password = loginAlert.textFields![1].text ?? ""
                
                completionHandler(.useCredential,
                                  URLCredential(user: username, password: password, persistence: .permanent))
            }))
            loginAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                self.cancelledAuth = true
                completionHandler(.cancelAuthenticationChallenge, nil)
            }))
            
            present(loginAlert, animated: true, completion: nil)
        } else {
            completionHandler(.performDefaultHandling, nil)
        }
    }
}

