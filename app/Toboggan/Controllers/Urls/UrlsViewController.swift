//
//  UrlsViewController.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-04-14.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

class UrlItem {
    var url: String
    var selected: Bool
    
    init(url: String, selected: Bool) {
        self.url = url
        self.selected = selected
    }
}

class UrlsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    @IBOutlet weak var newUrlTextfield: UITextField!
    @IBOutlet weak var urlsTableView: UITableView!
    
    public var delegate: UrlsViewControllerDelegate?
    
    private let initialSelectedUrl: String? = SettingsStorage.defaultUrl
    private var urlsMap: [UrlItem] = []
    
    private var selectedUrlItem: UrlItem? {
        get {
            return urlsMap.filter({ (item) -> Bool in
                return item.selected == true
            }).first
        }
    }
    
    private var currentSelectedIndex: Int {
        get {
            return urlsMap.index { (item) -> Bool in
                return item.selected == true
                } ?? 0
        }
    }
    
    private var urls: [String] {
        get {
            return urlsMap.map({ (item) -> String in
                return item.url
            })
        }
    }
    
    //
    // Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newUrlTextfield.delegate = self
        
        let defaultUrl = SettingsStorage.defaultUrl ?? ""
        for url in SettingsStorage.urls {
            let selected = (url == defaultUrl)
            urlsMap.append(UrlItem(url: url, selected: selected))
        }
        
        urlsTableView.dataSource = self
        urlsTableView.delegate = self
        //sender.titleLabel?.text = "Edit list"
        
        self.popoverPresentationController?.delegate = self
    }
    
    //
    // Methods
    //
    
    private func validateDone()-> Bool {
        if (urlsMap.isEmpty) {
            showOkAlert(title: "Error", message: "Add an URL to the list!")
            
            return false
        } else if (selectedUrlItem == nil) {
            showOkAlert(title: "Error", message: "Select an URL to load!")
            
            return false
        } else {
            if (initialSelectedUrl != selectedUrlItem?.url) {
                delegate?.selectedUrlChanged()
            }
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            
            return true
        }
    }
    
    private func persistChanges() {
        SettingsStorage.urls = urls
        SettingsStorage.defaultUrl = selectedUrlItem?.url
    }
    
    //
    // IBActions
    //
    
    @IBAction func addUrlButtonTapped() {
        let newUrl = newUrlTextfield.text ?? ""
        
        if (newUrl.isEmpty == false) {
            newUrlTextfield.text = ""
            
            let selected = urlsMap.isEmpty
            urlsMap.append(UrlItem(url: newUrl, selected: selected))
            urlsTableView.insertRows(at: [IndexPath(row: urlsMap.count-1, section: 0)], with: .automatic)
            
            persistChanges()
        }
    }
    
    @IBAction func onEditListButtonTapped(_ sender: UIButton) {
        urlsTableView.isEditing = !urlsTableView.isEditing
        
        if (urlsTableView.isEditing) {
            sender.setTitle("✓ End editing", for: .normal)
        } else {
            sender.setTitle("Edit list", for: .normal)
        }
    }
    @IBAction func a(_ sender: UIButton) {
        
    }
    
    @IBAction func onDoneButtonTapped(_ sender: Any) {
        let _ = validateDone()
    }
    
    //
    // UITextfieldDelegate
    //
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addUrlButtonTapped()
        return true
    }
    
    //
    // UITableViewDatasource
    //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urlsMap.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell.url", for: indexPath)
        
        let urlItem = urlsMap[indexPath.row]
        cell.textLabel?.text = urlItem.url
        
        if (urlItem.selected) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    //
    // UITableViewDelegate
    //
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let oldSelectedIndexPath = IndexPath(row: currentSelectedIndex, section: 0)
        
        selectedUrlItem?.selected = false
        urlsMap[indexPath.row].selected = true
        
        tableView.reloadRows(at: [oldSelectedIndexPath, indexPath], with: .automatic)
        
        persistChanges()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (_, actionIndexPath) in
            
            let deletedUrlItem = self.urlsMap.remove(at: actionIndexPath.row)
            
            // automatically selected first item in list if deleted item was selected
            if (deletedUrlItem.selected == true) {
                self.urlsMap.first?.selected = true
            }
            
            tableView.deleteRows(at: [actionIndexPath], with: .automatic)
            
            let newSelectedIndexPath = IndexPath(row: self.currentSelectedIndex, section: 0)
            tableView.reloadRows(at: [newSelectedIndexPath], with: .automatic)
            
            self.persistChanges()
        }
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedItem = urlsMap.remove(at: sourceIndexPath.row)
        urlsMap.insert(movedItem, at: destinationIndexPath.row)
        
        persistChanges()
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //
    // UIPopoverPresentationControllerDelegate
    //
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return validateDone()
    }
}
