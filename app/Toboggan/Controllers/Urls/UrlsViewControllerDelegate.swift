//
//  UrlsViewControllerDelegate.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-04-14.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

protocol UrlsViewControllerDelegate {
    func selectedUrlChanged()
}
