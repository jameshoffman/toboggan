//
//  UIViewControllerExtension.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-03-23.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    public func showOkAlert(title: String, message: String, handler: (()-> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
            handler?()
        }))
        
        present(alert, animated: true, completion: nil)
    }
}
