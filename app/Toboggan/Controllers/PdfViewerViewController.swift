//
//  PdfViewerViewController.swift
//  Toboggan
//
//  Created by James Hoffman on 2018-03-22.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation
import UIKit
import PDFKit
import SafariServices

class PdfViewerViewController: UIViewController, PDFViewDelegate {
    public var pdfUrl: URL!
    private let pdfView: PDFView = PDFView()
    
    // Handling status bar: https://stackoverflow.com/questions/32965610/hide-the-status-bar-in-ios-9/32965748
    private var statusBarHidden: Bool = false {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    //
    // Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = pdfUrl.deletingPathExtension().lastPathComponent
        
        // Add PDFView
        pdfView.minScaleFactor = 0.1
        pdfView.autoScales = true
        pdfView.displayMode = .singlePage
        pdfView.usePageViewController(true, withViewOptions: nil)
        pdfView.delegate = self
        pdfView.displayDirection = .horizontal
        
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.alpha = 0
        view.addSubview(pdfView)
        
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        // Taps
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(onPdfViewTapped(_:)))
        singleTap.numberOfTapsRequired = 1
        view.addGestureRecognizer(singleTap)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onPdfViewDoubleTapped(_:)))
        doubleTap.numberOfTapsRequired = 2
        pdfView.addGestureRecognizer(doubleTap)
        
        singleTap.require(toFail: doubleTap)
        
        // Swipes
        let forwardSwipe = UISwipeGestureRecognizer(target: self, action: #selector(onPdfViewSwippedForward))
        forwardSwipe.direction = .left
        pdfView.addGestureRecognizer(forwardSwipe)
        
        let backwardSwipe = UISwipeGestureRecognizer(target: self, action: #selector(onPdfViewSwippedBackward(_:)))
        backwardSwipe.direction = .right
        pdfView.addGestureRecognizer(backwardSwipe)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
        if (pdfView.document == nil) {
            do {
                pdfView.document = try PDFDocument(data: Data(contentsOf: pdfUrl))
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.pdfView.alpha = 1
                })
            } catch {
                showOkAlert(title: "Error", message: "Unable to open PDF from\n\(pdfUrl.absoluteString)", handler: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    //
    // Methods
    //
    private func changePdfScale(scale: CGFloat) {
        UIView.animate(withDuration: 0.3, animations: {
            self.pdfView.scaleFactor = scale
        })
    }
    
    private func scaleToFit() {
        changePdfScale(scale: self.pdfView.scaleFactorForSizeToFit)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil) { _ in
            self.scaleToFit()
        }
    }
    
    //
    // UIGestures targets
    //
    
    @objc private func onPdfViewTapped(_ sender: Any?) {
        CATransaction.begin()
            CATransaction.setCompletionBlock {
                self.scaleToFit()
            }
        
            statusBarHidden = !statusBarHidden
            navigationController?.setNavigationBarHidden(statusBarHidden, animated: true)
        
        CATransaction.commit()
    }
    
    @objc private func onPdfViewDoubleTapped(_ sender: Any?) {
        if (pdfView.scaleFactor == pdfView.scaleFactorForSizeToFit) {
            changePdfScale(scale: (pdfView.scaleFactor * 2))
        } else {
            scaleToFit()
        }
    }
    
    @objc private func onPdfViewSwippedBackward(_ sender: Any?) {
        pdfView.goToPreviousPage(nil)
    }
    
    @objc private func onPdfViewSwippedForward(_ sender: Any?) {
        pdfView.goToNextPage(nil)
    }
    
    //
    // PDFViewDelegate
    //
    
    func pdfViewWillClick(onLink sender: PDFView, with url: URL) {
        if (url.absoluteString.lowercased().starts(with: "http")) {
            let safariViewController = SFSafariViewController(url: url)
            safariViewController.preferredControlTintColor = navigationController?.navigationBar.tintColor
            present(safariViewController, animated: true, completion: nil)
        } else if (UIApplication.shared.canOpenURL(url)) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            showOkAlert(title: "Error", message: "Unable to open URL:\n\(url.absoluteString)")
        }
    }
}
